﻿using System;
using System.Linq;
using Seafarers.Persistence;
using System.Collections.Generic;

namespace Seafarers
{
    class Program
    {
        private static long ChooseWorld(IWorldRepository repository)
        {
            for (; ; )
            {
                Console.WriteLine("Choose World:");
                var worlds = repository.GetWorlds();
                foreach (var w in worlds)
                {
                    Console.WriteLine($"\t{w.WorldName}");
                }
                var input = Console.ReadLine();
                var world = worlds.SingleOrDefault(x => x.WorldName == input);
                if (world != null)
                {
                    return world.WorldId;
                }
                else
                {
                    Console.WriteLine("Not a valid world!");
                }
            }
        }
        private static long ChoosePlayer(IPlayerRepository repository)
        {
            for (; ; )
            {
                Console.WriteLine("Player Name?");
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Not a valid player name!");
                }
                else
                {
                    var player = repository.FindPlayer(input);
                    if (player != null)
                    {
                        return player.PlayerId;
                    }
                    else
                    {
                        Console.WriteLine("That player name doesn't exist! Create? (Y/N)");
                        bool done = false;
                        while (!done)
                        {
                            var key = Console.ReadKey(true).Key;
                            if (key == ConsoleKey.Y)
                            {
                                player = repository.CreatePlayer(input);
                                done = true;
                                return player.PlayerId;
                            }
                            else if (key == ConsoleKey.N)
                            {
                                done = true;
                            }
                        }
                    }
                }
            }
        }
        private static long FetchAvatar(IAvatarRepository repository, long playerId, long worldId)
        {
            var avatar = repository.FindAvatar(worldId, playerId);
            if (avatar != null)
            {
                return avatar.AvatarId;
            }
            else
            {
                avatar = repository.CreateAvatar(worldId, playerId);
                return avatar.AvatarId;
            }
        }
        private static void DoMove(IAvatarRepository avatarRepository, long avatarId)
        {
            var avatar = avatarRepository.FindAvatar(avatarId);
            avatar.X += Math.Cos(avatar.Heading) * avatar.Speed;
            avatar.Y += Math.Sin(avatar.Heading) * avatar.Speed;
            avatarRepository.UpdateAvatar(avatar);
        }
        private static void ChangeHeading(IAvatarRepository avatarRepository, long avatarId)
        {
            Console.WriteLine("New Heading:");
            var input = Console.ReadLine();
            if(Double.TryParse(input, out double value))
            {
                var avatar = avatarRepository.FindAvatar(avatarId);
                avatar.Heading = value;
                avatarRepository.UpdateAvatar(avatar);
            }
            else
            {
                Console.WriteLine("Not a valid heading, leaving old value!");
            }
        }
        private static void ChangeSpeed(IAvatarRepository avatarRepository, long avatarId)
        {
            Console.WriteLine("New Speed:");
            var input = Console.ReadLine();
            if(Double.TryParse(input, out double value))
            {
                var avatar = avatarRepository.FindAvatar(avatarId);
                avatar.Speed = Math.Min(Math.Max(value,0),1);
                avatarRepository.UpdateAvatar(avatar);
            }
            else
            {
                Console.WriteLine("Not a valid heading, leaving old value!");
            }
        }
        static void Main(string[] args)
        {
            IWorldRepository worldRepository = new WorldRepository();
            IAvatarRepository avatarRepository = new AvatarRepository();
            IPlayerRepository playerRepository = new PlayerRepository();
            var avatarId = FetchAvatar(avatarRepository, ChoosePlayer(playerRepository), ChooseWorld(worldRepository));
            bool done = false;
            while (!done)
            {
                var avatar = avatarRepository.FindAvatar(avatarId);
                Console.WriteLine($"X: {avatar.X}");
                Console.WriteLine($"Y: {avatar.Y}");
                Console.WriteLine($"Heading: {avatar.Heading}");
                Console.WriteLine($"Speed: {avatar.Speed}");

                var keys = new HashSet<ConsoleKey>();

                Console.WriteLine("[Q]uit");
                keys.Add(ConsoleKey.Q);

                Console.WriteLine("[M]ove");
                keys.Add(ConsoleKey.M);

                Console.WriteLine("Change [H]eading");
                keys.Add(ConsoleKey.H);

                Console.WriteLine("Change [S]peed");
                keys.Add(ConsoleKey.S);

                ConsoleKey key = default(ConsoleKey);
                do
                {
                    key = Console.ReadKey(true).Key;
                } while (!keys.Contains(key));

                switch (key)
                {
                    case ConsoleKey.Q:
                        done = true;
                        break;
                    case ConsoleKey.M:
                        DoMove(avatarRepository, avatarId);
                        break;
                    case ConsoleKey.H:
                        ChangeHeading(avatarRepository, avatarId);
                        break;
                    case ConsoleKey.S:
                        ChangeSpeed(avatarRepository, avatarId);
                        break;
                    default:
                        Console.WriteLine("I don't know that to do about that.");
                        break;
                }
            }
        }
    }
}
