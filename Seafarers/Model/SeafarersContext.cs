﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Seafarers.Entities;

namespace Seafarers.Model
{
    public partial class SeafarersContext : DbContext
    {
        public SeafarersContext()
        {
        }

        public SeafarersContext(DbContextOptions<SeafarersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Avatars> Avatars { get; set; }
        public virtual DbSet<Players> Players { get; set; }
        public virtual DbSet<Worlds> Worlds { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite("Data Source=../Data/seafarers.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avatars>(entity =>
            {
                entity.Property(e => e.AvatarId).ValueGeneratedOnAdd();

                entity.Property(e => e.Speed).HasDefaultValueSql("1.0");
            });

            modelBuilder.Entity<Players>(entity =>
            {
                entity.HasIndex(e => e.PlayerName)
                    .IsUnique();

                entity.Property(e => e.PlayerId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Worlds>(entity =>
            {
                entity.HasIndex(e => e.WorldName)
                    .IsUnique();

                entity.Property(e => e.WorldId).ValueGeneratedOnAdd();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
