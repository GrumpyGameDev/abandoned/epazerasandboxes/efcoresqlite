﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seafarers.Entities
{
    public partial class Worlds
    {
        [Key]
        public long WorldId { get; set; }
        [Required]
        public string WorldName { get; set; }
    }
}
