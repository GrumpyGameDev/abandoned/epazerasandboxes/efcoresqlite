﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seafarers.Entities
{
    public partial class Avatars
    {
        [Key]
        public long AvatarId { get; set; }
        public long PlayerId { get; set; }
        public long WorldId { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Heading { get; set; }
        public double Speed { get; set; }
    }
}
