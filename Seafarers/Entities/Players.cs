﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seafarers.Entities
{
    public partial class Players
    {
        [Key]
        public long PlayerId { get; set; }
        [Required]
        public string PlayerName { get; set; }
    }
}
