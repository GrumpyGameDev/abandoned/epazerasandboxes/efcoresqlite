using Seafarers.Entities;
using System.Collections.Generic;
using Seafarers.Model;
using System.Linq;
namespace Seafarers.Persistence
{
    public interface IAvatarRepository
    {
        Avatars FindAvatar(long avatarId);
        Avatars FindAvatar(long worldId, long playerId);
        Avatars CreateAvatar(long worldId, long playerId);
        bool UpdateAvatar(Avatars updatedAvatar);
    }
    public class AvatarRepository: IAvatarRepository
    {
        public Avatars FindAvatar(long worldId, long playerId)
        {
            using(var db = new SeafarersContext())
            {
                return db.Avatars.SingleOrDefault(x=>x.WorldId==worldId && x.PlayerId==playerId);
            }
        }

        public Avatars CreateAvatar(long worldId, long playerId)
        {
            using(var db = new SeafarersContext())
            {
                var avatar = new Avatars();
                avatar.WorldId=worldId;
                avatar.PlayerId=playerId;
                db.Add(avatar);
                db.SaveChanges();
                return avatar;
            }
        }
        public Avatars FindAvatar(long avatarId)
        {
            using(var db = new SeafarersContext())
            {
                return db.Avatars.SingleOrDefault(x=>x.AvatarId==avatarId);
            }
        }

        public bool UpdateAvatar(Avatars updatedAvatar)
        {
            using(var db = new SeafarersContext())
            {
                var avatar = db.Avatars.SingleOrDefault(x=>x.AvatarId==updatedAvatar.AvatarId);
                if(avatar!=null)
                {
                    avatar.Heading=updatedAvatar.Heading;
                    avatar.X = updatedAvatar.X;
                    avatar.Y = updatedAvatar.Y;
                    avatar.Speed = updatedAvatar.Speed;
                    db.Avatars.Update(avatar);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}