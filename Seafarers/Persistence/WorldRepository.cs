using Seafarers.Entities;
using System.Collections.Generic;
using Seafarers.Model;
using System.Linq;
namespace Seafarers.Persistence
{
    public interface IWorldRepository
    {
        IEnumerable<Worlds> GetWorlds();
    }
    public class WorldRepository: IWorldRepository
    {
        public IEnumerable<Worlds> GetWorlds()
        {
            using(var db = new SeafarersContext())
            {
                return db.Worlds.ToList();
            }
        }
    }
}