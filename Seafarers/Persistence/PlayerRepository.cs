using Seafarers.Entities;
using System.Collections.Generic;
using Seafarers.Model;
using System.Linq;
namespace Seafarers.Persistence
{
    public interface IPlayerRepository
    {
        Players FindPlayer(string playerName);
        Players CreatePlayer(string playerName);
    }
    public class PlayerRepository: IPlayerRepository
    {
        public Players FindPlayer(string playerName)
        {
            using(var db = new SeafarersContext())
            {
                return db.Players.SingleOrDefault(x=>x.PlayerName.Equals(playerName));
            }
        }

        public Players CreatePlayer(string playerName)
        {
            using(var db = new SeafarersContext())
            {
                var player = new Players();
                player.PlayerName = playerName;
                db.Add(player);
                db.SaveChanges();
                return player;
            }
        }

    }
}