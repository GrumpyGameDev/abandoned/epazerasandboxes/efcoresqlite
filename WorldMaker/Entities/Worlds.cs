﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldMaker.Entities
{
    public partial class Worlds
    {
        public Worlds()
        {
            Islands = new HashSet<Islands>();
        }

        [Key]
        public long WorldId { get; set; }
        [Required]
        public string WorldName { get; set; }

        [InverseProperty("World")]
        public virtual ICollection<Islands> Islands { get; set; }
    }
}
