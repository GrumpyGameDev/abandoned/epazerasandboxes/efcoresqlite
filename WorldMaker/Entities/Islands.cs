﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldMaker.Entities
{
    public partial class Islands
    {
        [Key]
        public long IslandId { get; set; }
        [Required]
        public string IslandName { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public long WorldId { get; set; }

        [ForeignKey(nameof(WorldId))]
        [InverseProperty(nameof(Worlds.Islands))]
        public virtual Worlds World { get; set; }
    }
}
