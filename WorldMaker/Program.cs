﻿using System;
using WorldMaker.Model;
using WorldMaker.Entities;
using System.Linq;
using System.Collections.Generic;

namespace WorldMaker
{
    class Program
    {
        const double WorldWidth = 1000;
        const double WorldHeight = 1000;
        const double IslandMinimumDistance = 10;
        const int GeneratorTries = 500;
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("WorldMaker <WORLD_NAME>");
            }
            var worldName = args[0];
            using (var db = new SeafarersContext())
            {
                //find or create world
                var world = db.Worlds.SingleOrDefault(x => x.WorldName == worldName);
                if (world == null)
                {
                    world = new Worlds();
                    world.WorldName = worldName;
                    db.Add(world);
                    db.SaveChanges();
                }

                Random random = new Random();
                //destroy existing islands
                var islands = db.Islands.Where(x => x.WorldId == world.WorldId).ToList();
                foreach (var i in islands)
                {
                    db.Islands.Remove(i);
                }
                db.SaveChanges();


                //create new islands
                islands = new List<Islands>();
                int currentTry = 0;
                while (currentTry < GeneratorTries)
                {
                    double x = random.NextDouble() * WorldWidth;
                    double y = random.NextDouble() * WorldHeight;
                    Console.WriteLine(currentTry);

                    if (islands.Any(i=>Math.Sqrt((i.X-x)*(i.X-x)+(i.Y-y)*(i.Y-y))<IslandMinimumDistance))
                    {
                        currentTry++;
                    }
                    else
                    {
                        Islands island = new Islands();
                        island.IslandName = Guid.NewGuid().ToString();
                        island.X = x;
                        island.Y = y;
                        islands.Add(island);
                        currentTry = 0;
                    }
                }

                foreach(var i in islands)
                {
                    db.Islands.Add(i);
                }
                db.SaveChanges();

            }
        }
    }
}
